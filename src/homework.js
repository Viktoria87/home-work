//прямоугольник
var horizCount = 10;  // определяем сколько символов будет занимать по горизонтали
var vertCount = 5;    // определяем сколько символов будет занимать по горизонтали
var star = '*';       // символ звездочки
var whitespace = '&nbsp&nbsp'; // символ пробела

// Делаем цикл сколько надо всего линий сделать по вертикали
for (var i = 0; i < vertCount; ++i) {
    var str = '';

    // если это первая линия или последняя, то рисуем чисто звездочки
    if (i == 0 || i == vertCount - 1) {    
      for (var j = 0; j < horizCount; ++j) {
        str += star;
      }    
      document.write("<br>");
    // если это ряд в середине, то в начале и конце рисуем звезды
    // а в середине - пробелы
    } else {
      str = star;
      for (var j = 0; j < horizCount - 2 ; ++j) {
        str += whitespace;
      }    
      str += star;
      document.write("<br>");
    }
    // вывод на консоль итога
    document.write(str);    
}




//прямоугольный триугольник
for (var h = 0; h < 8; h++) {
  for (var w = 0; w < h + 1; w++) {
    document.write("*\n");
  }
  document.write("<br>");
}


//равносторонний триугольник
var line = 8;  // кол-во знаков в линии
var space = 7; // кол-во зазоров в первой линии
var star = 1;  // кол-во звезд в первой линии

for (var h = 0; h < line; h++) {
  for (var wsp = 0; wsp < space; wsp++) {
    document.write("&nbsp\n");
  }
  for (var wst = 0; wst < star; wst++) {
    document.write("*");
  }
  space -= 1;
  star += 2;
  document.write("<br>");
}


//ромб
// Разделим график на две части и распечатаем, сначала распечатайте первые четыре строки
for(var i=1; i<=4; i++){
  // Печатать пробелы
  for(var j=1; j<=4-i;j++){
      document.write("&ensp;");
  }
  // Считаем * и пробел между ними как *
  // Распечатать*
  for(var k=1; k<=2*i-1; k++){
      if (k === 1 || k == 2 * i-1) {// Вывести * в первом и последнем столбце каждой строки и вывести пробелы для остальных
          document.write("*");
      } else{
          document.write("&ensp;");
      }
  }
  // новая линия
  document.write("<br/>");
}

// выводим последние три строки
for(var i=1; i<=3; i++){
  // пробелы для печати
  for(var j=1; j<=i; j++){
      document.write("&ensp;");
  }
  // выводим звездочку и пробел между ними
  for(var k=1; k<=7-2*i; k++){
      if (k === 1 || k == 7-2 * i) {// Первый и последний столбцы каждой строки печатают *, а остальные пробелы печати
          document.write("*");
      } else{
          document.write("&ensp;");
      }
  }
  document.write("<br/>");
}